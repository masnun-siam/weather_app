import 'dart:convert';

import 'package:get/get.dart';
import 'package:weather_app/models/weather_model.dart';
import 'package:http/http.dart';

class WeatherController extends GetxController {
  WeatherModel? weatherModel;

  bool isLoading = false;

  void getWeather(String cityName) async {
    isLoading = true;
    update();
    final url = Uri.parse(
        'https://api.openweathermap.org/data/2.5/weather?q=$cityName&appid=913ec3567b524aa420826cbc2cef5e1e&units=metric');

    final result = await get(url);
    isLoading = false;
    update();
    if (result.statusCode == 200) {
      final jsonData = result.body;
      final mapData = jsonDecode(jsonData);
      weatherModel = WeatherModel.fromJson(mapData);
      update();
    } else {
      Get.printError(info: 'Invalid response');
      weatherModel = WeatherModel();
      update();
    }
  }
}
