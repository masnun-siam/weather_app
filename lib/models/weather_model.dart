class WeatherModel {
  String? cityName;
  String? weatherDetails;
  double? temp;
  String? icon;

  WeatherModel({
    this.cityName,
    this.icon,
    this.temp,
    this.weatherDetails,
  });

  factory WeatherModel.fromJson(Map<String, dynamic> json) {
    return WeatherModel(
      cityName: json['name'],
      icon: ((json['weather'] as List?)?.isEmpty ?? true) ? null : 
      (json['weather'] as List).first['icon'],
      weatherDetails: ((json['weather'] as List?)?.isEmpty ?? true) ? null : 
      (json['weather'] as List).first['main'],
      temp: (json['main']['temp'] as num?)?.toDouble(),
    );
  }
}
