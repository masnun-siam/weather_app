import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:weather_app/controllers/weather_controller.dart';

class WeatherPage extends StatelessWidget {
  const WeatherPage({super.key });


  @override
  Widget build(BuildContext context) {
    final controller = Get.find<WeatherController>();
    return Scaffold(
      body: GetBuilder<WeatherController>(
        builder: (controller) => controller.isLoading 
        ? Center(child: CircularProgressIndicator()) 
        : Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(controller.weatherModel?.cityName?.toString() ?? ''),
            Text(controller.weatherModel?.weatherDetails?.toString() ?? ''),
            if (controller.weatherModel?.icon != null)
              Image.network(
                  'https://openweathermap.org/img/wn/${controller.weatherModel?.icon}@2x.png'),
            if (controller.weatherModel?.temp != null)
            Text("${controller.weatherModel?.temp}° C"),
            const SizedBox(width: double.infinity),
          ],
        ),
      ),
    );
  }
}
